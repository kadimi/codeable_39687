jQuery( document ).ready( function() {

	setInterval( function() {
		var $ = jQuery;
		var is_free = 'Free' === $.trim( $( '.packblock.selected .price' ).text() );
		var allow_multi = $( '#check_multi' ).val() === '1';

		if ( is_free || ! allow_multi ) {
			// Dim and uncheck sub-categories
			$( '#panel_section3 .children' ).css( 'opacity', .5 );
			$( '#panel_section3 .children :checkbox' ).attr( 'checked', false );
		} else {
			// Brighten sub-categories
			$( '#panel_section3 .children' ).css( 'opacity', 1 );
		}
	}, 100);
} );
